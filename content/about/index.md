---
title: "A propos d'Agnostic Whale"
date: 2020-05-20T18:37:37+02:00
menu: "about"
draft: false
author: "Fred"
---

## Qu'est-ce que Agnostic WHale

L'idée m'est venu suite à un ras le bol des réseaux sociaux, de leur politique de like, de la chronologie de l'information et de leurs trolls incessants.

J'ai donc décidé de monter ce blog, d'en faire un four-tout pour partager mes photos, mes coups de coeur et ma vie en général. Ce blog n'utilise aucun trackeur, il est full HTTPS et n'aura jamais de publicité.

Le site est hébergé chez [Netlify](https://www.netlify.com/) et généré via [Hugo](https://gohugo.io/). C'est une solution très simple à mettre en oeuvre et donne un site très rapide et très robuste.

## Qui suis-je

Je m'appel Fred, la quarantaine, marié à une adorable femme et papa de deux enfants. Je suis Architect Cloud indépendant dans la région nantaise. Je pratique la photographie en pur amateur, la plongée en scaphandre. J'aime la musique, particulièrement le "Metal" et plus généralement le "Rock".

<!-- {{< imgnetlify "DSC00228.jpg" "Photo prise par ma fille Manon, 9 ans" >}} -->

## On trouve quoi sur Agnostic Whale

Ici il n'y a rien à vendre, pas de test de matos ni d'article sponsorisé. Tu y trouveras principalement des photos, de la musique et mes retour d'expérience sur divers sujets.
