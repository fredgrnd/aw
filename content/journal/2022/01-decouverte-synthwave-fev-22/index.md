---
title: "Découverte Synthwave du mois de février"
date: 2022-02-22T09:55:14+02:00
menu: "musique"
draft: false
author: "Fred"
categories: "Musique"
summary: "Les découvertes syntwave du mois de février."
---

{{< bandcamp "4176672874" >}}
{{< bandcamp "3933686648" >}}
{{< bandcamp "2798716173" >}}
