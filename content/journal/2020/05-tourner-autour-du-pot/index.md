---
title: "Tourner autour du pot"
date: 2020-05-20T09:55:14+02:00
menu: "photo"
draft: false
author: "Fred"
categories: "Photos"
summary: "J'ai passé un après-midi à me ballader autour du Phare des Poulains pour m'imprégner du site et trouver les meilleurs cadrages."
---

Lorsque je pars faire des photos, je peux passer beaucoup de temps sur un sujet en particulier.

{{< imgnetlify "_DSC2013.jpg" >}}

J'ai passé un après-midi à me ballader autour du [Phare des Poulains](https://www.belle-ile.com/explorer/des-richesses-naturelles/la-pointe-des-poulains-et-sarah-bernhardt) pour m'imprégner du site et trouver les meilleurs cadrages.

{{< photoset max="2" >}}
{{< imgnetlify "_DSC2007-Enhanced.jpg" >}}
{{< imgnetlify "_DSC2037.jpg" >}}
{{</ photoset >}}

En Bretagne la lumière peut changer très vite, d'où l'intérêt d'essayer différent cadrage, de revenir sur ses pas et de prendre le temps...

{{< imgnetlify "_DSC2017.jpg" >}}

Prendre le temps de s'imprégner d'un lieu c'est aussi prendre le temps de se libérer des émotions que l'on peut ressentir lorsqu’on découvre un nouveau site et de se recentrer sur l'essentiel.

{{< photoset max="3" >}}
{{< imgnetlify "_DSC2004-Enhanced.jpg" >}}
{{< imgnetlify "_DSC2006.jpg" >}}
{{< imgnetlify "_DSC2040.jpg" >}}
{{</ photoset >}}
