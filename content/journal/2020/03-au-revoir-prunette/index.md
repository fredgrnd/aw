---
title: "Au revoir Prunette"
date: 2020-05-08T10:52:49+02:00
menu: "journal"
draft: false
author: Fred
categories: "journal"
summary: "Prunette, Nénette, Poupoune et même parfois Rantanplan... Tu nous à brusquement quitté cette semaine. Tu avais tout juste un an et encore tout un tas d'années devant toi."
---

Prunette, Nénette, Poupoune et même parfois Rantanplan... Tu nous à brusquement quitté cette semaine. Tu avais tout juste un an et encore tout un tas d'années devant toi.

{{< imgnetlify "_DSC2596.jpg" >}}

Tout est arrivé si soudainement. Tu laisses un gouffre dans nos cœurs, les jours se suivent et se ressemblent, on t’entend, on te voit, on ne t'oublie pas.

Manon t'as écrit une petite lettre, la voici

>Prune, prunette tu avais tant de prénoms tu étais si joyeuse de partir courir jusqu’à la route de fay et à la route de fay prune tu t’es fait  renverser tu as eu un choc et tu es morte on est tous triste de devoir te quitter si tôt tu n’avis que 1an et 1 mois  tu n’avais même pas deux ans  que tu n’étais plus avec nous nous sommes si tristes  que là j’écris ce  texte qui éprouve mes  sentiments pour toi prune ma 🐶 chienne adorée je t’aime tellement que je pleure depuis hier le mercredi 6 mai je t’aime 😍.
>Manon ta maîtresse qui t’aime de la part de toute la famille.

C'est le cœur plein de chagrin que nous te laissons rejoindre un monde meilleur, on t'aime, tu nous manque.

Ta famille
