---
title: "Ouverture"
date: 2020-05-01T10:52:49+02:00
menu: "journal"
draft: false
author: Fred
categories: "Life"
summary: "En ce premier jour de mai et accessoirement 3021ème jour de confinement je décide de monter un énième blog, je m'explique."
---

En ce premier jour de mai et accessoirement 3021ème jour de confinement je décide de monter un énième blog, je m'explique.

{{< imgnetlify "_DSC2368.jpg" "Manon, plage des salines Pilot, Black River, Ile Maurice" >}}

## Rester maître de ses données

C'est ma motivation première. Il n'y a plus de débat sur l'utilisation que font les réseaux sociaux de nos données, les CGU changent régulièrement et ce qui est vrai un jour ne l'ai pas forcément le lendemain. Sans parler de la remise en cause de la chronologie des posts qui ne permet pas de suivre les gens correctement.
Je postais jusqu'ici sur Instagram, Facebook et Twitter. Tout sera concentré désormais ici.

## Mettre un fin au troll

C'est ma motivation première (bis). Twitter et Facebook sont de vrais nids à troll, je ne compte pas les fois ou je me suis fait troller pour un malencontreux tweet et encore moins ceux qui m'ont sauté à la gorge parce que j'avais osé les contredire (intrépide, je suis). Je ne parle pas des pseudos moralisateurs, qu'ils soient fachos, écolos, féministes ou vegan qui dégueulent leur discours et pour qui la liberté de penser se résume à *"tu as le droit de penser ce que tu veux tant que tu penses comme moi"*.

Ce blog n'aura donc pas d'espace commentaire. Mes articles seront en sens unique, ça te plais tu me suis, ce ne te plais pas tu passes ton chemin.

## J'ai besoin d'écrire

Je ressens de plus en plus le besoin d'écrire, je n'ai pas de talent particulier, je suis très mauvais en grammaire et orthographe mais je pense qu'à force de persévérance je pourrais progresser.

## Pas de flicage, pas de pression

Pas de like, pas de compteur de visite à la *Google Analytics*, pas de commentaire. J'écris avant tout pour moi. Ce blog vivra quelques jours ou quelques années on verra.

## Et ça parlera de quoi

Ici, je vais te parler des mes passions autour de la musique, de la photo, de mon micro activisme sur la protection animale et environnementale, de la plongée en scaphandre et surement un peu aussi de nouvelles technologies.

## Du coup ça ne parlera pas de quoi

Je ne te parlerais jamais de politique ou de religion. Il n'y aura pas non plus de *tuto* pour apprendre à faire cuire les pâtes.

## Un système de blog simple et rapide

Pour ce blog j'utilise le framework [Hugo](https://gohugo.io/). Pour faire simple, je rédige mes articles au format [Markdown](https://docs.framasoft.org/fr/grav/markdown.html). Hugo se charge ensuite de le transformer au format HTML pour qu'il puisse s'afficher dans votre navigateur. Le code est stocké sur un dépôt privé [Gitlab](https://gitlab.com) et hébergé par [Netlify](https://www.netlify.com).
