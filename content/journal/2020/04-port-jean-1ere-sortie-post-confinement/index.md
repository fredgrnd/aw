---
title: "Port-jean, 1ère sortie post confinement"
date: 2020-05-16T12:00:00+02:00
menu: "journal"
draft: false
author: Fred
categories: "Photos"
summary: "J'ai pris mon vélo ce matin et j'ai filé sur les bords de l'Erdre avec mon mon matériel photo pour prendre quelques photos avec la lumière du matin."
---

C'était "la sortie" post confinement, une sorte de but à atteindre. Les bords de l'Erdre ont été fermés pendant toute la durée du confinement et, de toutes façons, c'est à 2km de chez nous...

{{< imgnetlify "DSC00622.jpg" >}}

J'ai pris mon vélo ce matin et j'ai filé là-bas avec mon tout mon matériel pour prendre quelques photos avec la lumière dans le dos.

{{< photoset max="3" >}}
{{< imgnetlify "DSC00654.jpg" >}}
{{< imgnetlify "DSC00633.jpg" >}}
{{< imgnetlify "DSC00645.jpg" >}}
{{</ photoset >}}

Hormis quelques joggeurs, il n'y avait pas encore beaucoup de monde, ça fait vraiment du bien des passer du temps au calme près de l'eau.

{{< imgnetlify "DSC00619.jpg" >}}
