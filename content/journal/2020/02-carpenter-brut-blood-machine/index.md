---
title: "Carpenter Brut - Blood Machine"
date: 2020-05-04T10:52:49+02:00
menu: "journal"
draft: false
author: Fred
categories: "Musique"
summary: "Carpenter Brut est un groupe français de synthwave"
---

[Carpenter Brut](http://carpenterbrut.com/) est un groupe français de [synthwave](https://bandcamp.com/tag/synthwave). On retrouve ici l'OST de [Blood Machine](http://bloodmachines.com/index.php/fr/), un film de science fiction dont voici le synopsis:

> Deux chasseurs traquent une machine qui tente de s’émanciper. Après l’avoir abattue, ils assistent à un phénomène mystique : le spectre d’une jeune femme s’arrache de la carcasse du vaisseau comme si elle avait une âme. Cherchant à comprendre la nature de ce spectre, ils entament une course-poursuite avec elle à travers l’espace.  

{{< bandcamp "4138149063" >}}

Comme d'habitude, cet album de Carpenter Brut est une vraie source de motivation. Visse ton casque sur la tête, monte le son et laisse toi aller !
