---
title: "Mes albums du moi de mai"
date: 2024-06-01T10:00:00+02:00
menu: "journal"
draft: true
author: Fred
categories: "Musique"
summary: "Ma sélection musicale de mai 2024."
---

Voici ma sélection musicale, on va trouver de tout, du stoner, du black, du death et même de l'indus avec Combichrist

{{< bandcamp "3751433818" >}}
{{< bandcamp "615090762" >}}
{{< bandcamp "2847907936" >}}
{{< bandcamp "3057097717" >}}
{{< bandcamp "2675443783" >}}
{{< bandcamp "2816276191" >}}
{{< bandcamp "2019465951" >}}
{{< bandcamp "1709052114" >}}
