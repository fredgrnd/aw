---
title: "Mes albums du moi de juin"
date: 2024-06-12T10:00:00+02:00
menu: "journal"
draft: true
author: Fred
categories: "Musique"
summary: "Ma sélection musicale de juin 2024."
---

Dans cette sélection nous allons retrouver du black (Houle), du black atmo (Lascar), du death plus ou moins brutal (As Patres, Ulcerate, Umbra Vitae)

{{< bandcamp "3254593340" >}}
{{< bandcamp "2840991035" >}}
{{< bandcamp "3323952004" >}}
{{< bandcamp "2362658314" >}}
{{< bandcamp "4145222687" >}}
